
one_word_dict = ['Family & Relationships',   
'Health',                    
'Science & Mathematics',   
'Entertainment & Music',  
'Computers & Internet',      
'Education & Reference',     
'Sports',                    
'Society & Culture',         
'Business & Finance',        
'Politics & Government']

one_word = [
       'Family',
       'Science',
       'Entertainment',
       'Computer',
       'Education',
       'Sports',
       'Society',
       'Finance',
       'Politics'
]

wordnet_labels = {
       'Family & Relationships':'a social unit living together',
       'Science & Mathematics':'a particular branch of scientific knowledge',
       'Entertainment & Music':'an activity that is diverting and that holds the attention',
       'Computers & Internet': 'a machine for performing calculations automatically',
       'Education & Reference':'knowledge acquired by learning and instruction',
       'Sports':'an active diversion requiring physical exertion and competition',
       'Society & Culture':'an extended social group having a distinctive cultural and economic organization',
       'Business & Finance':'the commercial activity of providing funds and capital',
       'Politics & Government':'the study of government of states and other political units'
       }

interpretation_labels = {
'Family & Relationships':'This text is about Family & Relationships',   
'Health':'This text is about Health',                    
'Science & Mathematics':'This text is about Science & Mathematics',   
'Entertainment & Music':'This text is about Entertainment & Music',  
'Computers & Internet':'This text is about Computers & Internet',      
'Education & Reference':'This text is about Education & Reference',     
'Sports':'This text is about Sports',                    
'Society & Culture':'This text is about Society & Culture',         
'Business & Finance':'This text is about Business & Finance',        
'Politics & Government':'This text is about Politics & Government' 
}