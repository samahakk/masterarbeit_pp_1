import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

dt = pd.read_csv('Reviews.csv', sep=',')

list(dt.columns.values)

dt['Score'] = dt['Score'].replace({1: "verry bad", 2: "bad", 3: "neutral", 4: "good", 5: "verry good"})

dt['Text'] = dt['Text'].str.strip()

dt['Score'].describe()


v_counts = dt['Score'].value_counts()
print(v_counts)


dt.to_csv('tars_full')

tars_sample = dt.groupby('Score')['Text'].apply(lambda s: s.sample(29769))
tars_sample.to_csv('tars_sample')


temp = pd.read_csv('tars_sample')
dt_numpy = temp['Text'].to_numpy()

counter_words = 0
counter_characters = 0
avg_wordws = 0
avg_characters = 0
index = dt_numpy.size

for x in dt_numpy:
    temp_words = len(x.split())
    counter_words = counter_words + temp_words

    temp_characters = len(x)
    counter_characters = counter_characters + temp_characters

avg_words = counter_words / index
avg_characters = counter_characters /index
print(f'Wörter: {counter_words} \nBuchstaben: {counter_characters} \navg_words: {int(avg_words)}\navg_characters: {int(avg_characters)} \nTrainingsdatenpunkte: {index}\n')




